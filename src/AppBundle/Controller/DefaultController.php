<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Employee;
use AppBundle\Entity\Customer;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $r =  new \ReflectionClass(Employee::class);
        $employee = $r->newInstanceArgs();
        $employee->setName('Bogan23');
        $employee->setSolary(12);
        $em->persist($employee);
        $em->flush();

       // $customer = $em->getRepository(Employee::class)->findAll();
       // dump($customer);

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);

    }
}
